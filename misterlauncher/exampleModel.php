<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table = 'payments';
    public $timestamps = false;

    protected $casts = [
        'data' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * Отношения Платеж - Сервер
     */
    public function server(){
        return $this->hasOne('App\Models\Servers', 'id', 'server_id');
    }
}
