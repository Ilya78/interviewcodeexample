<?php

namespace App\Http\Controllers;

use App\Models\Payments;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Qiwi\Api\BillPayments;

class PremiumController extends Controller
{
    public $prices = [
        'month' => config('premium.month'),
        '3month' => config('premium.3month'),
        'forever' => config('premium.forever')
    ];

    public function index(){
        return view('pages.premium');
    }

    public function prepare(Request $r){
        if(!Auth::check())
            return ['success' => false];
        if(!$r->has('type'))
            return ['success' => false];

        $u = Auth::user();

        // Проверяем приобретен премиум аккаунт или нет
        if($u->premium == 1 && Carbon::parse($u->premium_date) > Carbon::now()){
            return ['success' => false, 'alreadyPaid' => true];
        }

        // Если нету такого тарифа - шлем success - false
        $type = $r->type;
        if(empty(data_get($this->prices, $type, null)))
            return ['success' => false];

        $price = data_get($this->prices, $type);
        if($u->balance < $price){
            // На балансе недостаточно средств
            return ['success' => true, 'showPayType' => true];
        }else{
            // На балансе достаточно средств
            // Получаем дату окончания тарифа
            if($type == 'month'){
                $dateEnd = Carbon::now()->addMonth();
            }elseif($type == '3month'){
                $dateEnd = Carbon::now()->addMonths(3);
            }elseif($type == 'forever'){
                $dateEnd = Carbon::now()->addYears(10);
            }

            // Создаем запись о платеже
            $p = new Payments();
            $p->inv_id = 0;
            $p->server_id = 0;
            $p->user_id = $u->user_id;
            $p->type = 'premium';
            $p->multi_place = 0;
            $p->rating = 0;
            $p->color = '';
            $p->period = 0;
            $p->cost = number_format($price, 0, '', '');
            $p->earn = 0;
            $p->discount = 0;
            $p->status = 1;
            $p->utm_campaign = 0;
            $p->utm_source = '';
            $p->coupon = '';
            $p->time_add = time();
            $p->date = Carbon::now();
            $p->date_end = $dateEnd;
            $p->save();

            // Списываем деньги
            $u->balance -= $price;
            $u->premium = 1;
            $u->premium_date = $dateEnd;

            $u->save();

            return ['success' => true, 'showPayType' => false];
        }
    }

    /* 
        На балансе недостаточно средств. Создаем ордер для платежной системы 
    */
    public function pay(Request $r){
        if(!Auth::check())
            return ['success' => false, 'msg' => 'Требуется авторизация'];
        if(!$r->has('type'))
            return ['success' => false, 'msg' => 'Не передан обязательный параметр'];
        if(!$r->has('card_type'))
            return ['success' => false, 'msg' => 'Не передан обязательный параметр'];
        if(!$r->has('tariff'))
            return ['success' => false, 'msg' => 'Не передан обязательный параметр'];

        $u = Auth::user();
        
        $r->session()->forget("pay_server_id");
        $r->session()->forget("pay_type");
        $r->session()->forget("pay_rating");
        $r->session()->forget("pay_period");

        $r->session()->put('pay_server_id', 0);
        $r->session()->put('pay_type', 'premium');
        $r->session()->put('pay_rating', 0);
        $r->session()->put('pay_period', 0);

        // Проверяем приобретен премиум аккаунт или нет
        if($u->premium == 1 && Carbon::parse($u->premium_date) > Carbon::now()){
            return ['success' => false, 'alreadyPaid' => true];
        }

        // Если нету такого тарифа - шлем success - false
        $tariff = $r->tariff;
        if(empty(data_get($this->prices, $tariff, null)))
            return ['success' => false];

        
        $price = $this->prices[$tariff];

        // Получаем дату окончания тарифа
        if($tariff == 'month'){
            $dateEnd = Carbon::now()->addMonth();
        }elseif($tariff == '3month'){
            $dateEnd = Carbon::now()->addMonths(3);
        }elseif($tariff == 'forever'){
            $dateEnd = Carbon::now()->addYears(10);
        }

        // Создаем запись о платеже
        $p = new Payments();
        $p->inv_id = 0;
        $p->server_id = 0;
        $p->user_id = $u->user_id;
        $p->type = 'premium';
        $p->multi_place = 0;
        $p->rating = 0;
        $p->color = '';
        $p->period = 0;
        $p->cost = number_format($price, 0, '', '');
        $p->earn = 0;
        $p->discount = 0;
        $p->status = 2;
        $p->utm_campaign = 0;
        $p->utm_source = '';
        $p->coupon = '';
        $p->time_add = time();
        $p->date = Carbon::now();
        $p->date_end = $dateEnd;
        $p->save();

        // Задаем дату окончания премиума, но не ставим premium = 1, тк платеж еще не подтвержден
        $u->premium_date = $dateEnd;
        $u->save();

        // Генерируем описание и создаем подпись для платежа
        $desc = 'Покупка Premium аккаунта до '.$dateEnd->format('Y-m-d');
        $signature = $this->getFormSignature($p->id, 'RUB', $desc, $price, 'unitToken');

        // Генерируем ссылку для оплаты
        $url = $this->generatePaymentUrl($price, $desc, $signature, $p->id, $r->type, $r->card_type);

        return [
            'success' => true,
            'redirect_uri' => $url,
            'price' => $price
        ];
    }

    private function generatePaymentUrl($sum, $desc, $signature, $pid, $ptype, $card_type = null){
        /*
         * Изначально ps = unitpay, если выбрали qiwi - оплата через qiwi, если выбрали карту СНГ
         * то выбираем для оплаты qiwi, если выбрали карту зарубежного банка или сумма более 50 000
         * то выбираем unitpay
         */
        $ps = 'unitpay';
        if($ptype == 'qiwi'){
            $ps = 'qiwi';
        }
        if($ptype == 'card'){
            $ps = 'qiwi';
            if($card_type == 'unitpay')
                $ps = 'unitpay';
        }
        if($ptype == 'card' && $sum >= 50000){
            $ps = 'unitpay';
        }
        
        // Создаем платеж через Qiwi
        if($ps == 'qiwi'){
            $billPayments = new BillPayments(config('payment.qiwiSecret'));

            $fpt = ($ptype == 'qiwi') ? 'qw' : 'card';
            $fields = [
                'amount' => $sum,
                'currency' => 'RUB',
                'comment' => $desc,
                'expirationDateTime' => Carbon::now()->addHour()->format('c'),
                'email' => '',
                'account' => '',
                'customFields' => [
                    'paySourcesFilter' => $fpt
                ],
                'successUrl' => 'https://misterlauncher.org/pay/success/'
            ];

            /** @var BillPayments $billPayments */
            $response = $billPayments->createBill($pid, $fields);

            return data_get($response, 'payUrl');
        }else {
            // Создаем платеж через UnitPay
            $data = http_build_query([
                'sum' => $sum,
                'account' => $pid,
                'desc' => $desc,
                'currency' => 'RUB',
                'signature' => $signature
            ]);

            return 'https://unitpay.ru/pay/{shopId}/' . $ptype . '?' . $data . '&hideDesc=true&hideMenu=true';
        }
    }

    /*
     * Генерация подписи для платежа
     */
    private function getFormSignature($account, $currency, $desc, $sum, $secretKey) {
        $hashStr = $account.'{up}'.$currency.'{up}'.$desc.'{up}'.$sum.'{up}'.$secretKey;
        return hash('sha256', $hashStr);
    }
}
