<?php

namespace App\Http\Controllers\Withdraw;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Jobs\SkypeJob;
use App\Models\BuyingItems;
use App\Models\Weapons;
use App\User;
use Illuminate\Http\Request;
use GuzzleHttp\Client as Guzzle;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ShadowPay extends Controller
{
    protected $client;
    protected $token = 'secret_token';
    protected $item_name;
    protected $steamid;
    protected $balance = 0;
    protected $tradePartner;
    protected $tradeToken;
    protected $item;
    protected $type;
    protected $dbPrice;
    protected $dbFrom;
    protected $dbTo;
    protected $selectedItem;
    protected $tableId;

    public function __construct($item_name, $steamid = null)
    {
        $this->item_name = $item_name;
        $this->steamid = $steamid;
    }

    public function check(){
        if(!$user = Auth::user())
            return ['success' => false, 'msg' => 'Need log in'];

        $this->client = new Guzzle();

        $toTable = new BuyingItems();
        $toTable->market_hash_name = $this->item_name;
        $toTable->save();

        $this->tableId = $toTable->id;

        $balance = $this->client->post('https://shadowpay.com/api/v1/get_balance', [
            'form_params' => [
                'token' => $this->token
            ]
        ]);
        $balance = json_decode($balance->getBody()->getContents());
        if(!empty($balance) && data_get($balance, 'status') == 'success'){
            $this->balance = data_get($balance, 'balance', 0);
        }

        if($this->balance == 0){
            return ['success' => false, 'msg' => 'Balance empty'];
        }

        $this->getItemAndType();
        $price = $this->getPriceToRequest();
        if(!$price) {
            return ['success' => false, 'msg' => 'Price not found'];
        }

        $data['token'] = $this->token;
        $data['steam_market_hash_name'] = $this->item_name;
        $res = $this->client->post('https://shadowpay.com/api/v1/search_item', [
            'form_params' => $data
        ]);
        $res = json_decode($res->getBody()->getContents());
        $this->getTokenAndPartner();

        $dataRes = [];

        // Нашли такой предмет в продаже
        if(!empty($res) && data_get($res, 'status') == 'success'){
            $selected = null;
            // Ищем предмет, который подходит по нашим ценовым диапазонам
            foreach(data_get($res, 'data') as $item){
                if(data_get($item, 'price') < $this->dbTo){
                    $selected = data_get($item, 'id');
                    break;
                }
            }
            //Если нашли нужный нам предмет по цене
            if(!empty($selected)){
                $this->selectedItem = $selected;
                //Пытаемся его купить
                $result = $this->buy();

                // Если получилось купить
                if(!empty($result) && data_get($result, 'status') == 'success') {
                    $table = BuyingItems::find($this->tableId);
                    $table->json = json_encode($result);
                    $table->market = 'ShadowPay';
                    $table->status = 1;
                    $table->save();
                    return ['success' => true, 'type' => 'sended', 'data' => [
                        'trade_id' => data_get($result, 'trade_data.steam_trade_token')
                    ]];
                }else{
                    if(data_get($result, 'error_message', '') == 'not_found_item'){
                        // Ищем похожий
                        $dataRes = $this->checkSimilarItemsShadowpay();
                    }else{
                        // Если задан SteamID, то отправляем ему ошибку
                        if ($this->steamid) {
                            $dataRes = ['success' => false, 'msg' => data_get($result, 'error_message')];
                        }
                    }
                }
            }else{
                // Купить не удалось, ищем похожий
                $dataRes = $this->checkSimilarItemsShadowpay();
            }
        }elseif(!empty($res) && data_get($res, 'status') == 'error'){
            // Не нашли предмет в продаже или возникла ошибка
            if(!empty(data_get($res, 'error_message', null))){
                if(data_get($res, 'error_message') == 'not_found_item'){
                    // Ищем похожий
                    $dataRes = $this->checkSimilarItemsShadowpay();
                }else {
                    // Если задан SteamID, то отправляем ему ошибку
                    if ($this->steamid) {
                        $dataRes = ['success' => false, 'msg' => data_get($res, 'error_message')];
                    }
                }
            }
        }

        return $dataRes;

    }

    public function checkSimilarItemsShadowpay(){
        $data['token'] = $this->token;
        $data['steam_market_hash_name'] = $this->item_name;
        // Отправляем запрос на поиск похожих предметов
        $res = $this->client->post('https://shadowpay.com/api/v1/search_similar_items', [
            'form_params' => $data
        ]);
        $res = json_decode($res->getBody()->getContents());

        // Нашли похожий предмет
        if(!empty(data_get($res, 'data')) && data_get($res, 'status', 'error') == 'success'){
            $data_sim = [];
            if (!empty($this->steamid)) {
                $api = new ApiController();
                foreach (data_get($res, 'data', []) as $item){
                    $name = trim(preg_replace('/\s*\([^)]*\)/', "", data_get($item, 'steam_market_hash_name')));
                    $wp = Weapons::where('full_name', $name)->first();
                    if(!$wp) continue;
                    $data_sim[] = [
                        'weapon_name' => data_get($item, 'steam_market_hash_name'),
                        'weapon_img' => $api->getCdnImg($name, true, true, true),
                        'weapon_price' => data_get($item, 'price'),
                        'sp_id' => data_get($item, 'id'),
                        'sp_asset_id' => data_get($item, 'steam_asset_id')
                    ];
                }
                return ['success' => true, 'type' => 'similar', 'items' => $data_sim];
            }
        }else{
            return ['success' => false, 'msg' => 'Item Not found'];
        }
    }

    private function buy(){
        $result = $this->client->post('https://shadowpay.com/api/v1/buy_item', [
            'form_params' => [
                'token' => $this->token,
                'id' => $this->selectedItem,
                'steamid' => $this->steamid,
                'trade_token' => $this->tradeToken
            ]
        ]);
        return json_decode($result->getBody()->getContents());
    }

    private function getItemAndType(){
        preg_match('/\(([^()]*)\)/', $this->item_name, $m);
        $this->item = str_replace([' ('.$m[1].')', '  '], '', $this->item_name);
        $this->type = $m[1];
    }

    private function getTokenAndPartner(){
        if($this->steamid){
            $user = User::where('steamid', $this->steamid)->first();
            if($user){
                $trade_url = $user->trade_url;$query = parse_url($trade_url, PHP_URL_QUERY);
                parse_str($query, $params);
                if(!empty(data_get($params, 'token', null)) && !empty(data_get($params, 'partner', null))) {
                    $this->tradePartner = $params['partner'];
                    $this->tradeToken = $params['token'];
                }else{
                    die();
                }
            }else{
                die();
            }
        }else{
            $this->steamid = '76561198982704662';
            $this->tradePartner = 'tradepartner_id';
            $this->tradeToken = 'tradetoken_id';
        }
    }

    private function getPriceToRequest(){
        $weapon = Weapons::where('full_name', $this->item)->first();
        if($weapon){
            $type = 'standart';
            if (strpos($this->item, 'StatTrak') !== false) {
                $type = 'stattrak';
            }
            if (strpos($this->item, 'Souvenir') !== false) {
                $type = 'souvenir';
            }
            Log::info($type);

            $this->dbPrice = data_get($weapon, 'price.'.$type.'.'.$this->type, 0);
            $this->dbFrom = $this->dbPrice * (1 - 10 / 100);
            $this->dbTo = $this->dbPrice * (1 + 10 / 100);
            return true;
        }

        return false;
    }
}
