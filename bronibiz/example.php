<?php namespace BroniBiz\PaymentSystem\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use BroniBiz\Additional\Helpers\Logger;
use BroniBiz\Additional\Models\Setting;
use BroniBiz\PaymentSystem\Models\Payment;
use BroniBiz\PaymentSystem\Models\PaySetting;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use October\Rain\Exception\ApplicationException;
use RainLab\User\Facades\Auth;
use RainLab\User\Models\User;
/**
 * Payments Back-end Controller
 */
class Payments extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    private static $terminal = 'terminal';
    private static $password = 'password';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('BroniBiz.PaymentSystem', 'paymentsystem', 'payments');
    }

    public function refillBalance(){
        $request = App::make('request');

        if(!$user = Auth::getUser())
            return ['success' => false, 'msg' => 'Требуется авторизация'];

        if(!$request->has('amount'))
            return ['success' => false, 'msg' => 'Не передана сумма пополнения'];

        // Берем пс которая выбрана по дефолту
        $payment_system = Setting::get('pay_default');

        $amount = intval($request->amount);
        if($amount < 1)
            return ['success' => false, 'msg' => 'Сумма должна быть положительная'];

        $orderId = time();

        // Создаем новый платеж
        $pt = new Payment();
        $pt->user_id = $user->id;
        $pt->order_id = $orderId;
        $pt->type_id = 1;
        $pt->amount = $amount;

        // Реферальная система ( делал другой разработчик )
        if($request->has('replenishment')) {
            if (!empty($user->referral_code)) {
                $obReferralDate = Carbon::parse($user->referral_code_exp);
                if ($obReferralDate->gte(Carbon::now())) {
                    $pt->referral_code = $user->referral_code;
                    $pt->referral_data = ['replenishment' => $request->replenishment];
                }
            }
        }
        $pt->save();


        $payment = new \BroniBiz\PaymentSystem\Classes\Payment();

        // Создаем экземпляр класса с нужной ПС
        $acquiring = $payment->$payment_system($request->header('referer') ?? null);
        // Генерируем ссылку на оплату
        $data = $acquiring->generateUrl($amount, $orderId);

        if(data_get($data, 'success', false)){
            $pt->payment_id = data_get($data, 'payment_id');
            $json = $pt->json;
            data_set($json, 'responsePay', data_get($data, 'response', null));
            $pt->json = $json;
            $pt->save();

            // В системе присутствует логирование действий пользователя ( тк это может быть сотрудник ). Записываем вызванное действие
            Logger::addLog('Создан запрос на пополнение баланса', [
                'type' => Payment::class,
                'related_id' => $pt->id
            ]);

            return ['success' => true, 'redirect' => data_get($data, 'payment_url')];
        }else{
            return ['success' => false, 'msg' => 'Error'];
        }
    }

    public function confirmation($acquiring, $param){
        // Обработка callback от ПС
        $payment = new \BroniBiz\PaymentSystem\Classes\Payment();
        $acq = $payment->$acquiring();
        $data = $acq->confirmation($param);
        

        if(data_get($data, 'status') == 'confirmed'){
            // Платеж совершен с успехом
            return $payment->paymentConfirmed(data_get($data, 'orderId'), data_get($data, 'amount'), $param);
        }elseif(data_get($data, 'status') == 'refunded'){
            // Произведен возврат средств
            return $payment->paymentRefunded(data_get($data, 'orderId'), $param);
        }elseif(data_get($data, 'status') == 'rejected'){
            // Платеж отклонен
            return $payment->paymentRejected(data_get($data, 'orderId'), $param);
        }else{
            return false;
        }
    }

    // Callback от Тинькофф
    public function tinkoffConfirm(){
        $jsonRequest = file_get_contents('php://input');
        $param = json_decode($jsonRequest, true);

        $res = $this->confirmation('tinkoff', $param);

        exit('ОК');
    }

    // Callback от YooMoney
    public function yoomoneyConfirm(){
        $jsonRequest = file_get_contents('php://input');
        $param = json_decode($jsonRequest, true);

        return $this->confirmation('yoomoney', $param);
    }

    // Callback от Сбербанка
    public function sberbankConfirm(Request $request){
        return $this->confirmation('sberbank', $request->all());
    }

    // Пополнение количества СМС. производим отдельно
    public function refillSms(){
        $request = App::make('request');

        if(!$user = Auth::getUser())
            return ['success' => false, 'msg' => 'Требуется авторизация'];

        if(!$request->has('type'))
            return ['success' => false, 'msg' => 'Не передано количество смс'];

        $settings = $this->getSettings();
        if(empty(data_get($settings, 'sms_tariffs.'.$request->type, null)))
            return ['success' => false, 'msg' => 'Тариф не найден'];

        $orderId = time();
        $pt = new Payment();
        $pt->user_id = $user->id;
        $pt->order_id = $orderId;
        $pt->type_id = 3;
        $pt->amount = data_get($settings, 'sms_tariffs.'.$request->type.'.cost');
        $data = [
            'sms_count' => data_get($settings, 'sms_tariffs.'.$request->type.'.count')
        ];
        $pt->json = $data;
        $pt->save();

        $payment = new \BroniBiz\PaymentSystem\Classes\Payment();
        $tinkoff = $payment->tinkoff($request->header('referer') ?? null);
        $data = $tinkoff->generateUrl(data_get($settings, 'sms_tariffs.'.$request->type.'.cost'), $orderId);

        if(data_get($data, 'success', false)){
            $pt->payment_id = data_get($data, 'payment_id');
            $json = $pt->json;
            data_set($json, 'responsePay', data_get($data, 'response', null));
            $pt->json = $json;
            $pt->save();

            Logger::addLog('Создан запрос на покупку СМС пакета', [
                'type' => Payment::class,
                'related_id' => $pt->id
            ]);

            return ['success' => true, 'redirect' => data_get($data, 'payment_url')];
        }else{
            return ['success' => false, 'msg' => 'Error'];
        }
    }

    public function getSettings(){
        $settings = PaySetting::select('value')->where('item', 'bronibiz_payments_settings')->first();
        // unset($settings['value']);
        return $settings;
    }

    /*
    * Получаем историю пополнений
    */
    public function getHistory(){
        $request = App::make('request');
        if(!$user = Auth::getUser())
            return ['success' => false, 'msg' => 'Требуется авторизация'];

        $perpage = 15;
        if($request->has('perPage'))
            $perpage = $request->get('perPage');


        $res = Payment::where('user_id', $user->id)->orderBy('created_at', 'desc');

        if($request->has('type')){
            $res->where('type_id', $request->get('type'));
        }

        $res = $res->paginate($perpage);
        $res->makeHidden([
            'json',
            'tariff_id',
            'user_id'
        ]);

        return $res;
    }
}
